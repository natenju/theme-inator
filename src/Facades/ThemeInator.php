<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/11/2019 7:25 AM
 * @Updated: 3/11/2019 7:25 AM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\ThemeInator\Facades;


use Illuminate\Support\Facades\Facade;

class ThemeInator extends Facade {
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() {
        return 'themeInator';
    }
}