<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/11/2019 7:28 AM
 * @Updated: 3/11/2019 7:28 AM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\ThemeInator;


use Illuminate\Support\ServiceProvider;

/**
 * Class ThemeInatorServiceProvider
 *
 * @package Natenju\ThemeInator
 */
class ThemeInatorServiceProvider extends ServiceProvider {
    
    /**
     * Register services.
     *
     * @return void
     */
    public function register() { }
    
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() {
        $this->publishes(
            [
                __DIR__ . "/config/theme-inator.php" => config_path("theme-inator.php"),
            ],
            "theme-inator"
        );
    }
}
