<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/11/2019 7:40 AM
 * @Updated: 3/11/2019 7:40 AM
 * @Desc   : [DESCRIPTION]
 */
return [
    "themes" => [
        "location" => "/resources/views/",
        "default"  => "default",
    ],
];